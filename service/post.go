package service

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"github.com/sirupsen/logrus"
	pb "gitlab.com/blog_najot_talim/medium_post_service/genproto/post_service"
	"gitlab.com/blog_najot_talim/medium_post_service/storage"
	"gitlab.com/blog_najot_talim/medium_post_service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type PostService struct {
	pb.UnimplementedPostServiceServer
	storage storage.StorageI
	logger  *logrus.Logger
}

func NewPostService(strg storage.StorageI, logger *logrus.Logger) *PostService {
	return &PostService{
		storage: strg,
		logger:  logger,
	}
}

func (s *PostService) Create(ctx context.Context, req *pb.Post) (*pb.Post, error) {
	post, err := s.storage.Post().Create(&repo.Post{
		Title:       req.Title,
		Description: req.Description,
		ImageUrl:    req.ImageUrl,
		UserID:      req.UserId,
		CategoryID:  req.CategoryId,
	})

	if err != nil {
		s.logger.WithError(err).Error("failed to create post in create func")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return parsePostModel(post), nil
}

func parsePostModel(post *repo.Post) *pb.Post {
	return &pb.Post{
		Id:          post.ID,
		Title:       post.Title,
		Description: post.Description,
		ImageUrl:    post.ImageUrl,
		UserId:      post.UserID,
		CategoryId:  post.CategoryID,
		CreatedAt:   post.CreatedAt.Format(time.RFC3339),
		UpdatedAt:   post.UpdatedAt.Format(time.RFC3339),
		ViewsCount:  post.ViewsCount,
	}
}

func (s *PostService) Get(ctx context.Context, req *pb.GetPostRequest) (*pb.Post, error) {
	post, err := s.storage.Post().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to get post in create func")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parsePostModel(post), nil
}

func (s *PostService) GetAll(ctx context.Context, req *pb.GetAllPostsRequest) (*pb.GetAllPostsResponse, error) {
	result, err := s.storage.Post().GetAll(&repo.GetAllPostsParams{
		Limit:  req.Limit,
		Page:   req.Page,
		Search: req.Search,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create post in create func")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	response := pb.GetAllPostsResponse{
		Posts: make([]*pb.Post, 0),
		Count: result.Count,
	}

	for _, post := range result.Posts {
		response.Posts = append(response.Posts, parsePostModel(post))
	}

	return &response, nil
}

func (s *PostService) Update(ctx context.Context, req *pb.Post) (*pb.Post, error) {
	post, err := s.storage.Post().Update(&repo.Post{
		ID:          req.Id,
		Title:       req.Title,
		Description: req.Description,
		ImageUrl:    req.ImageUrl,
		UserID:      req.UserId,
		CategoryID:  req.CategoryId,
	})
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parsePostModel(post), nil
}

func (s *PostService) Delete(ctx context.Context, req *pb.GetPostRequest) (*emptypb.Empty, error) {
	err := s.storage.Post().Delete(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete post by id")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return &emptypb.Empty{}, nil
}
