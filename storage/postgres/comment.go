package postgres

import (
	"database/sql"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/blog_najot_talim/medium_post_service/storage/repo"
)

type commentRepo struct {
	db *sqlx.DB
}

func NewComment(db *sqlx.DB) repo.CommentStorageI {
	return &commentRepo{
		db: db,
	}
}

func (cr *commentRepo) Create(comment *repo.Comment) (*repo.Comment, error) {
	var updatedAt sql.NullTime

	query := `
		INSERT INTO comments(
			post_id,
			user_id,
			description
		) VALUES($1, $2, $3)
		RETURNING id, updated_at
	`

	row := cr.db.QueryRow(
		query,
		comment.PostID,
		comment.UserID,
		comment.Description,
	)

	err := row.Scan(
		&comment.ID,
		&updatedAt,
	)
	if err != nil {
		return nil, err
	}
	comment.UpdatedAt = &updatedAt.Time

	return comment, nil
}

func (cr *commentRepo) GetAll(params *repo.GetAllCommentsParams) (*repo.GetAllCommentsResult, error) {
	result := repo.GetAllCommentsResult{
		Comments: make([]*repo.Comment, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)

	filter := " WHERE true "

	query := `
		SELECT
			c.id,
			c.user_id,
			c.post_id,
			c.description,
			c.created_at,
			c.updated_at
		FROM comments c
		` + filter + `
		ORDER BY c.created_at desc` + limit

	rows, err := cr.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var c repo.Comment
		var updatedAt sql.NullTime
		err := rows.Scan(
			&c.ID,
			&c.UserID,
			&c.PostID,
			&c.Description,
			&c.CreatedAt,
			&updatedAt,
		)
		if err != nil {
			return nil, err
		}
		c.UpdatedAt = &updatedAt.Time
		result.Comments = append(result.Comments, &c)
	}

	queryCount := `
		SELECT count(1) FROM comments c
		 ` + filter
	err = cr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (cr *commentRepo) Update(comment *repo.Comment) (*repo.Comment, error) {
	var updatedAt sql.NullTime
	query := `update comments set description=$1, updated_at=$2 where id=$3
			returning updated_at
			`

	err := cr.db.QueryRow(query, comment.Description, time.Now(), comment.ID).Scan(&updatedAt)
	if err != nil {
		return nil, err
	}
	comment.UpdatedAt = &updatedAt.Time
	return comment, nil
}

func (cr *commentRepo) Delete(id int64) error {
	query := `delete from comments where id=$1`

	result, err := cr.db.Exec(query, id)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return sql.ErrNoRows
	}
	return nil
}
