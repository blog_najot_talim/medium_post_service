package repo

import "time"

type Comment struct {
	ID          int64
	UserID      int64
	PostID      int64
	Description string
	CreatedAt   time.Time
	UpdatedAt   *time.Time
}

type GetAllCommentsParams struct {
	Limit  int64
	Page   int64
	PostId int64
	Search string
}

type GetAllCommentsResult struct {
	Comments []*Comment
	Count    int32
}

type CommentStorageI interface {
	Create(c *Comment) (*Comment, error)
	GetAll(params *GetAllCommentsParams) (*GetAllCommentsResult, error)
	Update(c *Comment) (*Comment, error)
	Delete(id int64) error
}
